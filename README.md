# Serverless - AWS Node.js Typescript

This project has been generated using the `aws-nodejs-typescript` template from the [Serverless framework](https://www.serverless.com/).

For detailed instructions, please refer to the [documentation](https://www.serverless.com/framework/docs/providers/aws/).

## Installation/deployment instructions

Depending on your preferred package manager, follow the instructions below to deploy your project.

> **Requirements**: NodeJS `lts/fermium (v.14.15.0)`. If you're using [nvm](https://github.com/nvm-sh/nvm), run `nvm use` to ensure you're using the same Node version in local and in your lambda's runtime.

## Install Dependencies
### Using NPM

- Run npm i serverless -g to install the serverless framework
- Run `npm i` to install the project dependencies


### Using Yarn

- Run `yarn` to install the project dependencies

## Deploy
When you have made code changes, and you want to quickly upload your updated code to AWS Lambda or just change function configuration.
Run this command `sls deploy` it will deploy the entire service via CloudFormation.

For detailed instructions, please refer to the [documentation](https://www.serverless.com/framework/docs/providers/aws/cli-reference/deploy).

### Using NPM
- Run `npx sls deploy` to deploy this stack to AWS

### Using Yarn

- Run `yarn sls deploy` to deploy this stack to AWS

### Database
- Run `touch db.sqlite` to create a database
- Run npx knex migrate

## Test

This project contains a three lambda function triggered by an HTTP request made on the provisioned API Gateway REST API `/url-shortener` route with `POST` and `GET` method. The request body must be provided as `application/json`. The body structure is tested by API Gateway against `src/functions/shortlink/schema.ts` JSON-Schema definition: it must contain the `originalURL` property.

- requesting any other path than `/url` and `/{shortenerUrl}` with any other method than `POST` and `GET` will result in API Gateway returning a `403` HTTP error code
- sending a `POST` request to `/url` with a payload **not** containing a string property named `originalURL` will result in API Gateway returning a `400` HTTP error code
- sending a `POST` request to `/url` with a payload containing a string property named `originalURL` will result in API Gateway returning a `200` HTTP status code with an id
- sending a `GET` request to `/{id}` with a query parameter **not** containing a string property named `id` will result in API Gateway returning a `400` HTTP error code
- sending a `GET` request to `/{id}` with a query parameter containing a string property named `id` will result in API Gateway returning a `200` HTTP status code with a short link object
- sending a `GET` request to `/url/metrics/{id}` with a query parameter **not** containing a string property named `id` will result in API Gateway returning a `400` HTTP error code
- sending a `GET` request to `/url/metrics/{id}` with a query parameter containing a string property named `id` will result in API Gateway returning a `200` HTTP status code with a list of metrics


> :warning: once deployed, it will generate a **public** endpoint within your AWS account resources. Anybody with the URL can actively execute the API Gateway endpoint and the corresponding lambda.

### Locally

In order to test the shortlinks function locally, run the following command:

- `env-cmd -f .env serverless offline start` if you're using NPM
- `yarn env-cmd -f .env serverless offline start` if you're using Yarn

### Remotely

Copy and replace your `url` - found in Serverless `deploy` command output - and `originalURL` parameter in the following `curl` command in your terminal or in Postman to test your newly deployed application.

```
curl --location --request POST 'https://myApiEndpoint/dev/url' \
--header 'Content-Type: application/json' \
--data-raw '{
    "originalURL": "https://google.com/"
}'
```

## Project features

### Project structure

The project code base is mainly located within the `src` folder. This folder is divided in:

- `functions` - containing code base and configuration for your lambda functions
- `libs` - containing shared code base between your lambdas

```
.
├── src
│   ├── functions               # Lambda configuration and source code folder
│   │   ├── shorturl
│   │       ├── handler.ts      # `Short-Url` lambda source code
│   │       ├── index.ts        # `Short-Url` lambda Serverless lambda configurations 
│   │       └── schema.ts       # `Short-Url` lambda input event JSON-Schema
│   │   
│   │
│   ├── libs                    # Lambda shared code
│   │   └── apiGateway.ts       # API Gateway specific helpers
│   │   └── handlerResolver.ts  # Sharable library for resolving lambda handlers
│   │   └── lambda.ts           # Lambda middleware
│   │   └── database.ts         # database configuration
│   │   └── aws-partial.ts      # Lambda middleware
│   │   └── lambda.ts           # Lambda middleware
│   │   └── lambda.ts           # Lambda middleware
│   │
│   └── database                # Database shared code
│       └── migrations          # Database changes or updates, creating or dropping tables
│       └── model               # Database table instances
│       
│
├── package.json
├── serverless.ts               # Serverless service file
├── tsconfig.json               # Typescript compiler configuration
├── tsconfig.paths.json         # Typescript paths
└── webpack.config.js           # Webpack configuration
```

### 3rd party libraries

- [json-schema-to-ts](https://github.com/ThomasAribart/json-schema-to-ts) - uses JSON-Schema definitions used by API Gateway for HTTP request validation to statically generate TypeScript types in your lambda's handler code base
- [middy](https://github.com/middyjs/middy) - middleware engine for Node.Js lambda. This project uses [http-json-body-parser](https://github.com/middyjs/middy/tree/master/packages/http-json-body-parser) to convert API Gateway `event.body` property, originally passed as a stringified JSON, to its corresponding parsed object
- [@serverless/typescript](https://github.com/serverless/typescript) - provides TypeScript definitions for `serverless.ts` service file
- [objectionJS](https://github.com/serverless/typescript) - provides TypeScript definitions for `serverless.ts` service file
- [yup](https://vincit.github.io/objection.js/) - - provides a runtime value parsing and validation.

### Advanced usage

Any tsconfig.json can be used, but if you do, set the environment variable `TS_NODE_CONFIG` for building the application, eg `TS_NODE_CONFIG=./tsconfig.app.json npx serverless webpack`
