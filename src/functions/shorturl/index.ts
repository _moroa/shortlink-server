import {handlerPath} from '@libs/handler-resolver';
import {AWSPartial} from "@libs/aws-partial";


export const URL_SHORTENER_URL = "/url"
export const urlShortener: AWSPartial = {
    functions: {
        createUrl: {
            handler: `${handlerPath(__dirname)}/handler.createUrl`,
            events: [
                {
                    httpApi: {
                        method: 'post',
                        path: URL_SHORTENER_URL,
                    },
                },
            ],
        },
        getUrl: {
            handler: `${handlerPath(__dirname)}/handler.getUrl`,
            events: [
                {
                    httpApi: {
                        method: 'get',
                        path: `/{id}`,
                    },
                },
            ],
        },
        getUrlMetrics: {
            handler: `${handlerPath(__dirname)}/handler.getUrlMetrics`,
            events: [
                {
                    httpApi: {
                        method: 'get',
                        path: `${URL_SHORTENER_URL}/metrics/{id}`,
                    },
                },
            ],
        }
    }


};
