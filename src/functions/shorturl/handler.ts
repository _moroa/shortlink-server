import type {ValidatedEventAPIGatewayProxyEvent} from '@libs/api-gateway';
import {formatBadRequestJSONResponse, formatJSONResponse, formatRedirectJSONResponse, formatUnprocessableEntityJSONResponse} from '@libs/api-gateway';
import {middyfy} from '@libs/lambda';
import * as yup from 'yup';
import * as shorten from 'shortlink';
import {fromString} from 'uuidv4';
import { validate } from 'uuid';

import schema from './schema';
import {UrlModel} from "../../database/model/url-model";
import {MetricModel} from "../../database/model/metric-model";
import {Model} from "objection";
import dbConfig from "@libs/database";
import {HTTP_BAD_REQUEST_ERROR, HTTP_RESOURCE_NOT_FOUND_ERROR, HTTP_UNPROCESSABLE_ENTITY_ERROR} from "../../constants/http-error-message";


//database connection instance.
Model.knex(dbConfig);

const HOST = "http://localhost:8080" //@todo find a better way to extract the endpoint

//===========================================
// CREATE RESOURCE
//===========================================
const create: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {

    //===========================================
    // VALIDATION

    const validator = yup.object().shape({
        originalURL: yup
            .string()
            .required()
            .url()
    })

    if (!event.body) {
        return formatBadRequestJSONResponse({
            message: HTTP_BAD_REQUEST_ERROR,
            errors: ["url is required"],
        });
    }


    validator.validate(event.body).catch(error => {
        return formatBadRequestJSONResponse({
            message: `The URL is not valid, make sure the URL you tried to shorten is correct.`,
            errors: error.errors,
        });
    })

    //===========================================
    // EXECUTE

    const {originalURL} = event.body
    const ctx = {
        id: fromString(originalURL),
        shorten_url: `${HOST}/${shorten.generate(8)}`,
        original_url: originalURL
    }

    // create database instance of url if doesn't exist
    return UrlModel.query().findOne("original_url", originalURL).then(it => {
        // handle duplicate url
        if (it) {
            return formatJSONResponse({
                message: `success`,
                ...{
                    id: it.id,
                    shortenUrl: it.shorten_url,
                }
            })
        } else {
            // insert new resource
            return UrlModel.query().insertAndFetch(ctx).then(entity => {
                const res = {
                    message: `success`,
                    id: entity.id,
                    shortenUrl: entity.shorten_url,
                }
                return formatJSONResponse(res)
            }).catch(err => {
                return formatUnprocessableEntityJSONResponse({
                    message: HTTP_UNPROCESSABLE_ENTITY_ERROR,
                    error: err
                });
            })
        }
    }).catch(err => {
        return formatUnprocessableEntityJSONResponse({
            message: HTTP_UNPROCESSABLE_ENTITY_ERROR,
            error: err
        });
    })
};

//===========================================
// RETRIEVE RESOURCE: ORIGINAL URL
//===========================================
// @ts-ignore
const get: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    //===========================================
    // VALIDATION

    if (!event.pathParameters.id) {
        return formatBadRequestJSONResponse({
            message: HTTP_BAD_REQUEST_ERROR,
            error: ["url is required"],
        });
    }


    const pattern = new RegExp('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
    if (!pattern.test(event.pathParameters.id)) {
        return formatBadRequestJSONResponse({
            message: HTTP_BAD_REQUEST_ERROR,
        });
    }

    //===========================================
    // EXECUTE

    const {id} = event.pathParameters
    return UrlModel.query().findOne("shorten_url", `${HOST}/${id}`).then(it => {
        if (it) {
            const ctx = {
                agent: event.requestContext["http"]["userAgent"],
                ip: event.requestContext["http"]["sourceIp"],
                url_id: it.id,
                type: "CLICK_EVENT"
            }

            // tracking clicks
            return MetricModel.query().insert(ctx).then(() => {
                return formatRedirectJSONResponse(it.original_url)
            }).catch(err => {
                return formatUnprocessableEntityJSONResponse({
                    message: HTTP_UNPROCESSABLE_ENTITY_ERROR,
                    error: err
                });
            })
        }

        return formatBadRequestJSONResponse({
            message: HTTP_RESOURCE_NOT_FOUND_ERROR,
        });

    }).catch(err => {
        return formatUnprocessableEntityJSONResponse({
            message: HTTP_UNPROCESSABLE_ENTITY_ERROR,
            error: err
        });
    })
}
//===========================================
// RETRIEVE RESOURCE: METRICS
//===========================================
const fetchMetrics: ValidatedEventAPIGatewayProxyEvent<typeof schema> = async (event) => {
    //===========================================
    // VALIDATION

    if (!event.pathParameters.id) {
        return formatBadRequestJSONResponse({
            message: HTTP_BAD_REQUEST_ERROR,
            errors: ["url is required"],
        });
    }

    if (!validate(event.pathParameters.id)) {
        return formatBadRequestJSONResponse({
            message: HTTP_BAD_REQUEST_ERROR,
        });
    }

    //===========================================
    // EXECUTE

    const {id} = event.pathParameters
    return UrlModel.query().findById(id).withGraphFetched("[metrics]").then(it => {
        if (it) {
            const res = {
                message: `success`,
                id: id,
                shortUrl: it.shorten_url,
                originalURL: it.original_url,
                metrics: it.metrics.map(it1 => ({
                    id: it1.id,
                    agent: it1.agent,
                    ip: it1.ip,
                    createdAt: it1.created_at,
                    type: it1.type
                }))
            }
            return formatJSONResponse(res)
        }

        throw Error(HTTP_RESOURCE_NOT_FOUND_ERROR)
    }).catch(err => {
        return formatUnprocessableEntityJSONResponse({
            message: HTTP_RESOURCE_NOT_FOUND_ERROR,
            error: err
        });
    })
}

export const createUrl = middyfy(create);
export const getUrl = middyfy(get);
export const getUrlMetrics = middyfy(fetchMetrics);

