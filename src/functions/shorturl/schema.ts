export default {
  type: "object",
  properties: {
    originalURL: { type: 'string' }
  },
  required: ['originalURL']
} as const;
