import {Model} from "objection";

export class MetricModel extends Model {

    id: string
    agent: string
    ip: string
    type: string
    created_at: string;

    metrics: MetricModel[]

    static get tableName() {
        return 'metrics';
    }

    static jsonSchema = {
        type: 'object',
        required: ['ip','agent','url_id'],
        properties: {
            id: {type: 'string'},
            ip: {type: 'string'},
            agent: {type: 'string'},
            type: {type: 'string'},
            url_id: {type: 'string'},
            createdAt: {type: 'string'},
        }
    };

    static relationMappings = () => ({
        url: {
            relation: Model.HasOneRelation,
            modelClass: MetricModel,
            join: {
                to: 'urls.id',
                from: 'metrics.url_id'
            }
        },
    })
}