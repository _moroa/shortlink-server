import {Model} from "objection";
import {MetricModel} from "./metric-model";

export class UrlModel extends Model {

    id: string
    shorten_url: string
    original_url: string
    created_at: string;
    updated_at: string;
    deleted_at?: string;

    metrics: MetricModel[]

    static get tableName() {
        return 'urls';
    }

    static jsonSchema = {
        type: 'object',
        required: ['shorten_url','original_url'],
        properties: {
            id: {type: 'string'},
            shorten_url: {type: 'string'},
            original_url: {type: 'string'},
            createdAt: {type: 'string'},
            updatedAt: {type: 'string'},
            deletedAt: {type: 'string', nullable: true},
        }
    };

    static relationMappings = () => ({
        metrics: {
            relation: Model.HasManyRelation,
            modelClass: MetricModel,
            join: {
                from: 'urls.id',
                to: 'metrics.url_id'
            }
        },
    })



}