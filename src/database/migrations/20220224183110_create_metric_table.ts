import { Knex } from "knex";
import {UUIDGeneratorSQL} from "@libs/database";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("metrics", function (table) {
        table.uuid("id").primary().defaultTo(knex.raw(UUIDGeneratorSQL))
        table.string("ip",255).notNullable()
        table.string("type").notNullable()
        table.text("agent").notNullable()
        table.uuid("url_id").references("urls.id").notNullable()
        table.timestamp("created_at").defaultTo(knex.fn.now());
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("metrics");
}
