import { Knex } from "knex";
import {UUIDGeneratorSQL} from "@libs/database";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable("urls", function (table) {
        table.uuid("id").primary().defaultTo(knex.raw(UUIDGeneratorSQL))
        table.string("shorten_url",255).notNullable()
        table.string("original_url",255).notNullable()
        table.timestamp("created_at").defaultTo(knex.fn.now());
        table.timestamp("updated_at").defaultTo(knex.fn.now());
        table.timestamp("deleted_at").nullable();
        // table.foreign('logoId').references('file.id')
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable("urls");

}

