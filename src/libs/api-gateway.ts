import type { APIGatewayProxyEvent, APIGatewayProxyResult, Handler } from "aws-lambda"
import type { FromSchema } from "json-schema-to-ts";

type ValidatedAPIGatewayProxyEvent<S> = Omit<APIGatewayProxyEvent, 'body'> & { body: FromSchema<S> }
export type ValidatedEventAPIGatewayProxyEvent<S> = Handler<ValidatedAPIGatewayProxyEvent<S>, APIGatewayProxyResult>

export const formatJSONResponse = (response: Record<string, unknown>) => {
  return {
    statusCode: 200,
    body: JSON.stringify(response)
  }
}

export const formatRedirectJSONResponse = (response: string) => {
  return {
    statusCode: 301,
    headers: {
      Location: response,
    }
  }
}

export const formatBadRequestJSONResponse = (response: Record<string, unknown>) => {
  return {
    statusCode: 400,
    body: JSON.stringify(response)
  }
}

export const formatUnprocessableEntityJSONResponse = (response: Record<string, unknown>) => {
  return {
    statusCode: 422,
    body: JSON.stringify(response)

  }
}

export const formatNotFoundJSONResponse = (response: Record<string, unknown>) => {
  return {
    statusCode: 404,
    body: JSON.stringify(response)
  }
}