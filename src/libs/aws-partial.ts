import {AWS} from "@serverless/typescript";

export type AWSPartial = Omit<Partial<AWS>, 'provider'> & { provider?: Partial<AWS['provider']> };
