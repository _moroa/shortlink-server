import Knex from "knex";

export const UUIDGeneratorSQL = `(lower(hex(randomblob(4))) || '-' || lower(hex(randomblob(2))) || '-4' || substr(lower(hex(randomblob(2))),2) || '-' || substr('89ab',abs(random()) % 4 + 1, 1) || substr(lower(hex(randomblob(2))),2) || '-' || lower(hex(randomblob(6))))`


const dbConfig = Knex({
    client: 'sqlite3',
    connection: () => ({
        filename: "./db.sqlite"
    }),
    useNullAsDefault: true,
    pool: {
        max: 1000,
        min: 10,
    },
});

export default dbConfig