// Update with your config settings.

import * as path from "path";


console.log( process.env.SQLITE_FILENAME)

module.exports = {
  client: 'sqlite3',
  connection: () => ({
    filename: "./db.sqlite"
  }),
  migrations: {
    tableName: "migrations",
    directory: path.resolve(path.join(__dirname, "/src/database/migrations")),
    extension: "ts",
  },
  seeds: {
    directory: path.resolve(path.join(__dirname, "/src/database/seeds")),
    extension: "ts",
  },
  // ...knexSnakeCaseMappers(),
  useNullAsDefault: true,
  pool: {
    max: 1000,
    min: 10,
  },
};

